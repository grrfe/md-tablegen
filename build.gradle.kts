plugins {
    kotlin("jvm") version "1.9.24"
    java
    id("net.nemerosa.versioning") version "3.1.0"
}

group = "fe.md-tablegen"
version = versioning.info.tag ?: versioning.info.full ?: "0.0.0"

repositories {
    mavenCentral()
}

kotlin {
    jvmToolchain(17)
}

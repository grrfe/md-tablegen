package fe.markdown.tablegen


class TableGenerator(private val delimiter: String = "|", private val lineBreak: String = "\n") {
    companion object {
        const val markdownBackticks = "```"
        const val spacerChar = "-"

        fun padColumn(length: Int, padMode: PadMode, string: String) = "%${padMode.modifier}${length}s".format(string)
    }

    enum class PadMode(val modifier: String) {
        Left(""), Right("-")
    }

    fun generate(header: Row, rows: List<Row>, footer: Row? = null): String {
        val columnLengths = mutableMapOf<Int, Int>()

        header.columns.indices.forEach { index ->
            columnLengths[index] = header.columnLength(index)
        }

        (rows + footer).forEach {
            header.columns.indices.forEach { index ->
                with(it?.columnLength(index) ?: 0) {
                    if (this > (columnLengths[index] ?: 0)) {
                        columnLengths[index] = this
                    }
                }
            }
        }

        return buildString {
            this.append(markdownBackticks)

            singleRow(header.columns, columnLengths, this)
            makeSpacer(columnLengths, this)

            rows.forEach { row ->
                singleRow(row.columns, columnLengths, this)
            }

            if (footer != null) {
                makeSpacer(columnLengths, this)
                singleRow(footer.columns, columnLengths, this)
            }

            this.append(lineBreak).append(markdownBackticks)
        }
    }

    private fun newLineDelimiter(sb: StringBuilder) = sb.also {
        it.append(lineBreak).append(delimiter)
    }

    private fun singleRow(columns: Array<out Column?>, lengths: Map<Int, Int>, sb: StringBuilder): StringBuilder {
        newLineDelimiter(sb).append(" ")
        columns.forEachIndexed { index, column ->
            appendColumn(column, lengths, index, sb)
        }

        return sb
    }

    private fun makeSpacer(lengths: Map<Int, Int>, sb: StringBuilder): StringBuilder {
        newLineDelimiter(sb)
        lengths.forEach { (k, _) ->
            sb.append(spacerChar.repeat(lengths[k]!! + 2)).append(delimiter)
        }

        return sb
    }

    private fun appendColumn(column: Column?, lengths: Map<Int, Int>, index: Int, sb: StringBuilder): StringBuilder {
        val length = lengths[index]!!
        if (column == null) {
            sb.append(" ".repeat(length + 3))
        } else sb.append(column.padTo(length)).append(" ").append(delimiter).append(" ")

        return sb
    }
}

data class Column(
    val value: String,
    val numeric: Boolean = false,
    val padMode: TableGenerator.PadMode = TableGenerator.PadMode.Right
) {
    fun length() = value.length
    fun padTo(length: Int) = TableGenerator.padColumn(length, padMode, value)
}

class Row(vararg val columns: Column? = arrayOf()) {
    fun columnLength(columnIndex: Int) = columns[columnIndex]?.length() ?: 0
}
